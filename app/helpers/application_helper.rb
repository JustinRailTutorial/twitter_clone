module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '') 
    base_title = "Ruby on Rails Twitter Clone App"  #variable assignment
    if page_title.empty?
      base_title                                    #implicit return
    else
      "#{page_title} | #{base_title}"               #string interpolation
    end
  end
end
